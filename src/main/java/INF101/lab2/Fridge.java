package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> itemsInFridge;

    public Fridge() {
        itemsInFridge = new ArrayList<>();
    } 


    @Override
    public int nItemsInFridge() {
        int nr = itemsInFridge.size();
        return nr;
    }

    @Override
    public int totalSize() {
        int maxCap = 20;
        return maxCap;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemsInFridge.size() < totalSize()) {
            itemsInFridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!itemsInFridge.contains(item)) {
            throw new NoSuchElementException("This element is not in the fridge!");
        }
        itemsInFridge.remove(item);
    
        
        
    }

    @Override
    public void emptyFridge() {
         
        itemsInFridge.clear();
        
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> goodFood;
        goodFood = new ArrayList<FridgeItem>();
        for (FridgeItem n : itemsInFridge) {
            if (!n.hasExpired()) {
                goodFood.add(n);
            }
        }
        itemsInFridge.removeAll(goodFood);
        return itemsInFridge;
    }
    
    
}
